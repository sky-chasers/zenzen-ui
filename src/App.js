import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Outlet } from 'react-router-dom';
import NavBar from './components/nav-bar/NavBar';
import CreatePostPage from './pages/create-post-page/CreatePostPage';
import UploadPostPage from './pages/upload-post-page/UploadPostPage';
import HomePage from './pages/home-page/HomePage';
import ViewPostPage from './pages/view-post-page/ViewPostPage';

function App() {
  return (
    <div className='App'>
      <BrowserRouter>
        <NavBar/>
        <Outlet />
            <Routes>
                <Route index element={<HomePage/>}/>
                <Route path='/post' element={<CreatePostPage/>}/>
                <Route path='/upload-images' element={<UploadPostPage/>}/>
                <Route path='/post/:id' element={<ViewPostPage/>}/>
            </Routes>
        </BrowserRouter>
    </div>
  );
}

export default App;
