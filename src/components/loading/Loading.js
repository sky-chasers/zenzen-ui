import './Loading.css';

function Loading() {
    return <div class="dot-flashing"></div>;
}

export default Loading;