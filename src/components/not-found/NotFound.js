import { TbFaceIdError } from "react-icons/tb";
import { Link } from "react-router-dom";
import './NotFound.css';

function NotFound() {
    return <div className="not-found-container">
        <TbFaceIdError className="not-found-icon"/>
        <div className="tag-line">
            Oops, we cant seem to find the page you're looking for. <br/>
            Why don't you head back <Link to="/">home</Link>.
        </div>
    </div>
}

export default NotFound;