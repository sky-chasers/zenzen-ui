/* eslint-disable jsx-a11y/img-redundant-alt */
import { useEffect, useState } from 'react';
import './ImageAlbum.css';

function ImageAlbum({images}) {

    let thumbnails;
    const [selectedImage, setSelectedImage] = useState('');

    useEffect(() => {
        if(images && images.length > 0) {
            setSelectedImage(images[0]);
        }
    }, []);

    if(images && images.length > 0) {
        thumbnails = images.map((imageLink, index) =>
            <img src={imageLink} onClick={() => setSelectedImage(imageLink)} key={index} alt="thumbnail" className="thumbnail"/>
        );
    }


    if(selectedImage && images && images.length > 0) {
        return <div className="image-album">
            <div className="selected-image">
                <img src={selectedImage} alt="selected image"/>
            </div>

            <div className="thumbnails">
                {thumbnails}
            </div>
        </div>;
    }

    return <div></div>;


}

export default ImageAlbum;