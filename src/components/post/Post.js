import './Post.css';
import PostType from '../../shared-services/PostTypes';
import dateFormat from 'dateformat';
import ImageAlbum from '../image-album/ImageAlbum';
import ShareButton from '../share-button/ShareButton';

function Post({post}) {
    return <div className="post">
        <ShareButton postId={post.id}/>

        <ImageAlbum images={post.images}/>

        <div className="title">{post.title}</div>

        <div className="location">Location: {post.city}, {post.country}</div>
        
        <div className="inserted-at">{dateFormat(post.insertedAt, "dddd, mmmm dS, yyyy, h:MM:ss TT")}</div>

        <div className="post-type">Post Type: {PostType.getPostTypeById(post.postType).name}</div>

        <div className="description">{post.description}</div>

        <div className="email-address">
            <a href={"mailto:" + post.emailAddress}>
                {post.emailAddress}
            </a>
        </div>

        <div className="phone-number">{post.phoneNumber}</div>

        <div className="contact-name">{post.contactName}</div>
    </div>;
}

export default Post;