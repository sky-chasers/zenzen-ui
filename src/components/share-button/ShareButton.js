import { useRef, useState } from 'react';
import { BiLink } from 'react-icons/bi';
import NuggetModal from '../nugget-modal/NuggetModal';
import './ShareButton.css';

function ShareButton({postId}){

    let [isModalShown, setIsModalShown] = useState(false);
    let hideModalTimer = useRef(null);

    function handleShareButtonClick() {
        _copyLinkOnClick(postId);
        setIsModalShown(true);  
        _closeModalAfterFiveSeconds();
    }

    function _closeModalAfterFiveSeconds() {
        clearTimeout(hideModalTimer);
        hideModalTimer = setTimeout(() => {
            setIsModalShown(false);
        }, 5000);
    }

    function _copyLinkOnClick(postId) {
        navigator.clipboard.writeText(_constructUrl(postId));
    }

    function _constructUrl(postId) {
        const baseUrl = window.location.origin.toString();
        return baseUrl + '/post/' + postId;
    }

    return <>
        <div className="share-button-container">
            <BiLink className="share-button" onClick={handleShareButtonClick}/>
        </div>
        <NuggetModal 
            textBody={"Link copied to clipboard."} 
            isModalShown={isModalShown} 
            handleClose={()=> setIsModalShown(false)} />
    </>; 
}

export default ShareButton;
