import './NavBar.css';
import { Outlet, Link } from 'react-router-dom';

function NavBar() {
    return <div className="nav-bar">
        <ul>
            <li className="logo">
                <Link to="/">adxn<span className="dot">.</span></Link>
            </li>
        </ul>

        <div className="add-post-button">
            <a href="/post">+</a>
        </div>
        
        <Outlet />
    </div>;
}

export default NavBar;