import { useEffect, useState } from 'react';
import { IoClose } from 'react-icons/io5';
import './NuggetModal.css';

function NuggetModal({textBody, isModalShown, handleClose}) {

    return <div className={`nugget-container ${isModalShown ? "show": "hide"}`}>
        <div className="text-body">
            {textBody}
        </div> 
        <div className="close-button-container">
            <IoClose className="close-button" onClick={handleClose}/>
        </div>
    </div>
}

export default NuggetModal;