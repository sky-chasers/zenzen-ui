
import './HomePage.css';
import { useEffect, useState } from 'react';
import Post from '../../components/post/Post';
import Loading from '../../components/loading/Loading';
import ReactPaginate from 'react-paginate';

function HomePage(){
    const INITIAL_OFFSET = 0;
    const ITEMS_PER_PAGE = 5;
    
    const [currentPage, setCurrentPage] = useState(0);
    const [pageCount, setPageCount] = useState(0);
    const [posts, setPosts] = useState([]);
    const [totalPostCount, setTotalPostCount] = useState(0);
    const [isLoading, setIsLoading] = useState(true);
    const ZENZEN_BACKEND_BASE_URL = process.env.REACT_APP_ZENZEN_BACKEND_BASE_URL;

    useEffect(() => {
        const end = calculateNewEnd(INITIAL_OFFSET);
        fetchPosts(INITIAL_OFFSET, end);
    }, []);

    function fetchPosts(start, end) {
        setIsLoading(true);
        const getPostsUrl = `${ZENZEN_BACKEND_BASE_URL}/posts?start=${start}&end=${end}`;

        fetch(getPostsUrl)
        .then(response => response.json())
        .then((response) => { 
            setPosts(response.data);
            setIsLoading(false);
            setTotalPostCount(response.totalCount);
            setPageCount(calculatePageCount(response.totalCount));  
        });
    }

    const onPageChange = (event) => {
        const newOffset = (event.selected * ITEMS_PER_PAGE) % totalPostCount;
        const newEnd = calculateNewEnd(newOffset);
        setCurrentPage(event.selected);
        fetchPosts(newOffset, newEnd);
    }

    function calculatePageCount(totalCount) {
        return Math.ceil(totalCount / ITEMS_PER_PAGE);
    }

    function calculateNewEnd(offset) {
        return offset + (ITEMS_PER_PAGE - 1);
    }

    if(isLoading) {
        return <div className="loading-bar-container">
            <Loading/>
        </div>;
    }

    return <div className="home-page">
        <div className="posts">
            { posts && posts.map((eachPost) => <Post post={eachPost} key={eachPost.id}/> )}
        </div>

        <ReactPaginate 
            breakLabel="..."
            nextLabel="next >"
            onPageChange={onPageChange}
            pageRangeDisplayed={5}
            pageCount={pageCount}
            forcePage={currentPage}
            previousLabel="< previous"
            renderOnZeroPageCount={null}
            containerClassName="pagination-container"
            pageClassName="page-item"
            pageLinkClassName="page-link"
            activeClassName="active-page-link"
            nextLinkClassName="page-link"
            previousClassName="page-link"
        />
    </div>;
}

export default HomePage;