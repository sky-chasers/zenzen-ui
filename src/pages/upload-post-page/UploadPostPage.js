/* eslint-disable jsx-a11y/img-redundant-alt */
import { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import FileBase64 from 'react-file-base64';
import './UploadPostPage.css';

function UploadImagesPage() {

    const location = useLocation();
    const navigate = useNavigate();
    const [post, setPost] = useState([]);
    const [imagesSelected, setImagesSelected] = useState(0);
    let [errorMessage, setErrorMessage] = useState('');
    let [disableSubmit, setDisableSubmit] = useState(false);
    let [isSubmitting, setIsSubmitting] = useState(false);
    const ZENZEN_BACKEND_BASE_URL = process.env.REACT_APP_ZENZEN_BACKEND_BASE_URL;

    useEffect(() => {
        if(location.state === null) {
            navigate('/post');
        }
        setPost(location.state);
    }, [location.state, navigate]);

    function allImagesHasValidFileType(images) {
        
        for (let image of images) {
            const {type, name} = image;

            if ( !(['image/jpg', 'image/jpeg', 'image/png'].includes(type)) ) {
                setDisableSubmit(true);
                setErrorMessage(`Invalid image type ${type} for image ${name}.`);
                console.error(`Invalid image type ${type} for image ${name}.`);
                return false;
            }
        }

        return true;
    }

    function handleFileUpload(images) {
        setErrorMessage('');
        setImagesSelected(images.length);
    
        if(!allImagesHasValidFileType(images)) {
            setErrorMessage('At least one image has invalid file type.');
            setDisableSubmit(true);
            console.error('At least one image has invalid file type.');
            return;
        }

        if(images.length > 3) {
            setDisableSubmit(true);
            setErrorMessage('You selected more than 3 images.');
            console.error('You selected more than 3 images.');
            return;
        }
        
        setDisableSubmit(false);
        post.images = extractImageBase64(images);
    }

    function extractImageBase64(images) {
        let imageList = [];

        images.forEach((image) => {
            imageList.push(image.base64);
        });

        return imageList;
    }

    function uploadPost(event) {
        setDisableSubmit(true);
        setIsSubmitting(true);
        const postUrl = `${ZENZEN_BACKEND_BASE_URL}/post`;
        event.preventDefault();

        const body = {
            "post": post
        };

        fetch(postUrl, { 
            headers: {'Content-Type': 'application/json'},
            method: 'POST', 
            body: JSON.stringify(body)
        })
        .then((response) => response.json())
        .then((result) => {
            navigate('/');
        })
        .catch((error) => {
            setErrorMessage('There was an error while processing your post. Try again later.');
        })
        .finally(() =>{
            setDisableSubmit(false);
            setIsSubmitting(false);
        });
    }

    return <div className="upload-image-container">
        <div className="banner">Upload an image!</div>

        <form onSubmit={uploadPost}>
            
            <div className="form-group">

                <img src={require('../../assets/images/image.png')} alt="generic picture of mountain"/>
                
                <div className={(errorMessage === '') ? '' : 'error-message'}>
                    {errorMessage}
                </div>

                <label className="input-image-button">
                    Upload
                    <FileBase64 multiple={true} onDone={handleFileUpload}></FileBase64>
                </label>

                <div className="images-selected">
                    {(imagesSelected > 0) ? imagesSelected + ' image(s) selected' : '' }
                </div>
            </div>
            
            <div className="instructions">
                You can upload a maximum of 3 images. <br/>
                We only accept PNG or JPEG.
            </div>

            <div className="submit-button-container">
                <input type="submit" className="submit-button" value={isSubmitting ? '...' : 'Submit' } disabled={disableSubmit}/>
            </div>
        </form>
        
    </div>
}

export default UploadImagesPage;