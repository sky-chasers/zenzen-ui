import { City, Country } from 'country-state-city';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import PostType from '../../shared-services/PostTypes';
import './CreatePostPage.css';

function PostPage() {

    const DEFAULT_COUNTRY = 'AF';
    const DEFAULT_CITY = 'Andkhoy';
    const DEFAULT_POST_TYPE = 'JOB_FULL_TIME';

    const navigate = useNavigate();

    const [countries, setCountries] = useState([]);
    const [cities, setCities] = useState([]);
    const [postTypes, setPostTypes] = useState([]);
    
    const [enableNextButton, setEnableNextButton] = useState(false);
    
    const [title, setTitle] = useState('');
    const [country, setCountry] = useState(DEFAULT_COUNTRY);
    const [city, setCity] = useState(DEFAULT_CITY);
    const [description, setDescription] = useState('');
    const [emailAddress, setEmailAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [contactName, setContactName] = useState('');
    const [postType, setPostType] = useState(DEFAULT_POST_TYPE);

    useEffect(() => {
        setCountries(Country.getAllCountries());
        setCities(City.getCitiesOfCountry(DEFAULT_COUNTRY));
        setPostTypes(PostType.POST_TYPES);
    }, []);

    function submitPost(event) {
        event.preventDefault();
        
        const post = {
            title: title,
            country: Country.getCountryByCode(country).name,
            city: city,
            description: description,
            emailAddress: emailAddress,
            phoneNumber: phoneNumber,
            contactName: contactName,
            postType: postType
        };

        navigate('/upload-images', { state: post });
    }

    function updateTitle(event) {
        setTitle(event.target.value);

        if(isNextButtonEnabled()) {
            setEnableNextButton(true);
        }
    }
    
    function updateSelectedCountry(event) {
        const countryCode = event.target.value;
        setCountry(countryCode);
        setCities(City.getCitiesOfCountry(countryCode));
        setCity(cities[0].name);
    }

    function updateDescription(event) {
        setDescription(event.target.value);

        if(isNextButtonEnabled()) {
            setEnableNextButton(true);
        }
    }

    function updateCity(event) {
        setCity(event.target.value);
    }

    function updateEmailAddress(event) {
        setEmailAddress(event.target.value);
    }

    function updatePhoneNumber(event) {
        setPhoneNumber(event.target.value);
    }

    function updateContactName(event) {
        setContactName(event.target.value);
    }

    function updatePostType(event) {
        setPostType(event.target.value);
    }

    function isNextButtonEnabled() {
        return title.length > 0 && description.length > 0;
    }

    return <div className="post-form">
        <form onSubmit={submitPost}>
            <div className="form-group">
                <label>
                    <input type="text" value={title} placeholder="*Title" className="title-input-box" name="title" onChange={updateTitle}/>
                </label>
            </div>

            <div className="form-group">
                <label>
                    <select name="post-type" value={postType} className="post-type-box" onChange={updatePostType}>
                        {postTypes.map((postType) => (
                            <option value={postType.id} key={postType.id}>{postType.name}</option>
                        ))}
                    </select>
                </label>
            </div>

            <div className="form-group">
                <label>
                    <select name="country" value={country} className="country-input-box" onChange={updateSelectedCountry}>
                        {countries.map((country) => (
                            <option value={country.isoCode} key={country.isoCode}>{country.name}</option>
                        ))}
                    </select>
                </label>
            </div>
            
            <div className="form-group">
                <label>
                    <select name="city" value={city} className="city-input-box" onChange={updateCity}>
                        {cities.map((city, index) => (
                            <option value={city.name} key={index}>{city.name}</option>
                        ))}
                    </select>
                </label>
            </div>
        
            <div className="form-group">
                <label>
                    <textarea type="text" value={description} placeholder="*Description" className="description-text-area" name="description" onChange={updateDescription} rows="5"/>
                </label>
            </div>

            <div className="form-group">
                <label>
                    <input type="text" value={emailAddress} placeholder="Email Address (will be shared with public)" className="email-input-box" name="email-address" onChange={updateEmailAddress}/>
                </label>
            </div>
            
            <div className="form-group">
                <label>
                    <input type="text" value={phoneNumber} placeholder="Phone Number (will be shared with public)" className="phone-input-box" name="phone-number" onChange={updatePhoneNumber}/>
                </label>
            </div>
            
            <div className="form-group">
                <label>
                    <input type="text" value={contactName} placeholder="Contact Name" className="contact-name-input-box" name="contact-name" onChange={updateContactName}/>
                </label>
            </div>

            <div className="button-group">
                <input type="submit" className="submit-button" value="Next" disabled={!enableNextButton}/>
            </div>
        </form>
    </div>
}

export default PostPage;