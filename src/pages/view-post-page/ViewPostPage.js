import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Loading from '../../components/loading/Loading';
import NotFound from '../../components/not-found/NotFound';
import Post from '../../components/post/Post';
import './ViewPostPage.css';

function ViewPostPage() {

    const {id} =  useParams();
    
    const [post, setPost] = useState();
    const [isLoading, setIsLoading] = useState(true);

    const ZENZEN_BACKEND_BASE_URL = process.env.REACT_APP_ZENZEN_BACKEND_BASE_URL;

    useEffect(() => {
        fetchPost(id);
    }, []);

    function fetchPost(postId) {
        setIsLoading(true);
        const getPostUrl = `${ZENZEN_BACKEND_BASE_URL}/post?id=${postId}`;

        fetch(getPostUrl)
        .then(response => response.json())
        .then((response) => { 
            setPost(response.data);  
        })
        .finally(() => {
            setIsLoading(false);
        });
    }

    if(isLoading) {
        return <div className="loading-bar-container">
            <Loading/>
        </div>;
    }else {
        if(post) {
            return <Post post={post}/>;
        } else {
            return <NotFound/>;
        }
    }
}

export default ViewPostPage;