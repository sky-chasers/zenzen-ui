const PostType = {
    POST_TYPES: [
        {
            "name": "Full Time Job",
            "id": "JOB_FULL_TIME"
        },
        {
            "name": "Part Time Job",
            "id": "JOB_PART_TIME"
        },
        {
            "name": "Rental Housing",
            "id": "RENTAL_HOUSING"
        },
        {
            "name": "Rental Item",
            "id": "RENTAL_ITEM"
        },
        {
            "name": "For Sale",
            "id": "FOR_SALE"
        },
        {
            "name": "Service",
            "id": "SERVICE"
        },
        {
            "name": "Community",
            "id": "COMMUNITY"
        },
        {
            "name": "Event",
            "id": "EVENT"
        },
        {
            "name": "Other",
            "id": "OTHER"
        }
    ],

    getPostTypeById: function(value) {
        return this.POST_TYPES.find((type) => value === type.id);
    }

}

export default PostType;